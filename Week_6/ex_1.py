name = input("Enter file:")
if len(name) < 1 : name = "mbox-short.txt"
handle = open(name)

dct = {}
for line in handle:
    if line.startswith("From "):
        word = line.split()
        time = word[5].split(":")
        hour = time[0]
        dct[hour] = dct.get(hour, 0) + 1
                                                    
lst = []
for h, c in dct.items():
    tup = (h, c)
    lst.append(tup)
                                                                
lst.sort()

for h, c in lst:
    print(h, c)

