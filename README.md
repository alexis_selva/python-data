These are the programming assignments relative to Python for everybody - data structures (2nd part)

- Week 3: Chapter Seven: Files
- Week 4: Chapter Eight: Lists
- Week 5: Chapter Nine: Dictionaries
- Week 6: Chapter Ten: Tuples

For more information, I invite you to have a look at https://www.coursera.org/learn/python-data
