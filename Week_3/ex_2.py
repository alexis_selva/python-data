value_f = 0.0
count = 0
fname = input("Enter file name: ")
fh = open(fname)
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    count = count + 1
    pos = line.find(":")
    value_f = value_f + float(line[pos + 1:])
print("Average spam confidence:", value_f/count)

