name = input("Enter file:")
if len(name) < 1 : name = "mbox-short.txt"
handle = open(name)

dct = {}
for line in handle:
    if line.startswith("From "):
        word = line.split()
        sender = word[1]
        dct[sender] = dct.get(sender, 0) + 1

prolific_key = None
prolific_value = None
for k, v in dct.items():
    if prolific_value == None:
        prolific_value = v
        prolific_key = k
    elif v > prolific_value:
        prolific_value = v
        prolific_key = k

print(prolific_key, prolific_value)
                                                                                             
                                                                                               

